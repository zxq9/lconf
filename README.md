# Live Conf
A simple live configuration application for systems that need a subscribable, living
configuration system.

## Usage
This application provides four main functions plus a convenience function that can load a configuration at startup.

The function reference can be found here:
[Live Conf docs](https://zxq9.com/projects/lconf/docs)


## How to include this in your project

### Using from ZX
Just add the dep from the OTPR realm:
```
   zx set dep otpr-lconf-0.1.0
```

### Playing with a git clone
```
    ceverett@okonomiyaki:~$ git clone https://gitlab.com/zxq9/lconf.git
    Cloning into 'lconf'...
# ...
    ceverett@okonomiyaki:~$ cd lconf/
    ceverett@okonomiyaki:~/lconf$ erl -pa ebin
    Erlang/OTP 24 [erts-12.1.5] [source] [64-bit] [smp:2:2] [ds:2:2:10] [async-threads:1] [jit]
    
    Eshell V12.1.5  (abort with ^G)
    1> make:all().
    Recompile: src/lconf_sup
    Recompile: src/lconf_man
    Recompile: src/lconf
    up_to_date
    2> lconf:start().
    ok
    3> lconf:save(foo, 1).
    ok
    4> lconf:read(foo).
    {ok,1}
    5> lconf:subscribe(foo).
    ok
    6> lconf:save(foo, 2).
    ok
    7> flush().
    Shell got {conf_update,foo,2}
    ok
```

### Using from rebar
Simply add this repo (ideally with a tag) to your rebar config and it should just work.
