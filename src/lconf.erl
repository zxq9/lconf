%%% @doc
%%% Live Conf
%%%
%%% This module is both the application and main interface module to lconf_man.
%%% @end

-module(lconf).
-vsn("0.1.0").
-behavior(application).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Interface functions
-export([save/2, read/1]).
-export([subscribe/1, unsubscribe/1]).

%% Startup convenience functions
-export([start/0, start/1]).

%% Application behavior callbacks
-export([start/2, stop/1]).



%%% Types

-export_type([key/0]).

-type key() :: term().



%%% Interface functions

-spec save(Key, Value) -> ok
    when Key   :: key(),
         Value :: term().
%% @doc
%% Save a value in the config manager, and broadcast the new value to all subscribed
%% processes if the value is different from the one already stored.
%%
%% Note that this is an async cast, so while you can be sure the value will be
%% propagated to any subscribed process, there is no guarantee about when those
%% processes will be able to read the message (if they are blocked on another
%% action or have an overloaded mailbox at the moment, for example).

save(Key, Value) ->
    lconf_man:save(Key, Value).


-spec read(Key) -> {ok, Value} | error
    when Key   :: key(),
         Value :: term().
%% @doc
%% Read a value from the config manager. The semantics are the same as `maps:find/2'.

read(Key) ->
    lconf_man:read(Key).


-spec subscribe(Key) -> ok | error
    when Key :: key().
%% @doc
%% Subscribe the calling process to the given Key.

subscribe(Key) ->
    lconf_man:subscribe(Key).


-spec unsubscribe(Key) -> ok
    when Key :: key().
%% @doc
%% Unsubscribe the calling process from the given Key.

unsubscribe(Key) ->
    lconf_man:unsubscribe(Key).


%%% Startup convenience functions

-spec start() -> ok.
%% @doc
%% Start the server in an "ignore" state.

start() ->
    ok = application:ensure_started(sasl),
    ok = application:start(lconf),
    ok.

-spec start(ConfigFile) -> ok
    when ConfigFile :: file:filename().
%% @doc
%% Start the application with a configuration file path as an initial argument.
%% This file should be readable using `file:consult/1' and should contain a list
%% of `{Key, Value}' pairs. This file will be read at startup and loaded into the
%% initial configuration.

start(ConfigFile) ->
    {ok, Values} = file:consult(ConfigFile),
    ok = start(),
    ok = lists:foreach(fun({K, V}) -> lconf_man:save(K, V) end, Values),
    io:format("Conf loaded from ~ts~n", [ConfigFile]).



%%% Application behvaior callbacks

-spec start(normal, term()) -> {ok, pid()}.
%% @private
%% Called by OTP to kick things off. This is for the use of the "application" part of
%% OTP, not to be called by user code.
%% See: http://erlang.org/doc/apps/kernel/application.html

start(normal, _Args) ->
    lconf_sup:start_link().


-spec stop(term()) -> ok.
%% @private
%% Similar to start/2 above, this is to be called by the "application" part of OTP,
%% not client code. Causes a (hopefully graceful) shutdown of the application.

stop(_State) ->
    ok.
