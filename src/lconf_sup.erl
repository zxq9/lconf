%%% @doc
%%% Live Conf Top-level Supervisor
%%%
%%% The very top level supervisor in the system. It only has one child: the
%%% conf manager procss.
%%% @end

-module(lconf_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).


-spec init([]) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init([]) ->
    RestartStrategy = {one_for_one, 1, 60},
    Conf      = {lconf_man,
                 {lconf_man, start_link, []},
                 permanent,
                 5000,
                 worker,
                 [lconf_man]},
    Children  = [Conf],
    {ok, {RestartStrategy, Children}}.
